import { Component } from '@angular/core';
import {FormControl} from '@angular/forms';

enum OPERATORS_ENUM {
  MINUS,
  PLUS,
  DIVISION,
  MULTIPLIER,
  RESULT
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public previousInputValue = '';
  public inputValue = new FormControl('');
  public activeOperator: OPERATORS_ENUM;
  public operator = OPERATORS_ENUM;

  handleButtonClick(value: number) {
    this.inputValue.setValue(this.inputValue.value + value);
  }

  handleOperatorClick(operator: OPERATORS_ENUM) {
    switch (operator) {
      case OPERATORS_ENUM.MINUS:
        this.activeOperator = operator;
        this.moveInputValueIntoPreviousInputValue();
        return;
      case OPERATORS_ENUM.RESULT:
        this.calculateResult();
    }
  }

  private moveInputValueIntoPreviousInputValue() {
    this.previousInputValue = this.inputValue.value;
    this.inputValue.setValue('');
  }

  private calculateResult() {
    const previousInputValue = parseInt(this.previousInputValue, 10);
    const currentInputValue = parseInt(this.inputValue.value, 10);
    switch (this.activeOperator) {
      case OPERATORS_ENUM.MINUS:
        this.inputValue.setValue(previousInputValue - currentInputValue);
    }
  }
}
